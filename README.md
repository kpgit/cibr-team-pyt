# READ ME
This is intended for homework-week09 assignment 3

### change log
* 0.1.0 initiate an empty calculator_branch.py file and README.md
* 0.2.0 add the main function
* 0.3.0 add the addtition function
* 0.4.0 add the subtraction function
* 0.5.0 add the division function
* 0.6.0 add the multiplication function
* 0.7.0 add the exponent function
* 1.0.0 publish the final code
